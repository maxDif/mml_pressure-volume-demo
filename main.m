clear; close all;

%% Data loading
load('data.mat');

% t: time
% krs, sigma0: parameters
% Non noisy data: Vol, Pres
% Noisy data: noisyVol, noisyPres
% indNoise: indices of the noisy points
% colorX: colors for display

%% Hyperparameters 

IN.numS = size(Vol,2); % Number of samples
IN.numDimMax = size(Vol,2)-1; % Max number of return dimensions
IN.alphaT = 1; % Diffusion Maps formalism if equal to 1
IN.t = 1; % diffusion time 

%% DM
IN.ksigma = 10;
IN.data = noisyPres;
tmp = diffusionMaps(IN);
cPres = tmp.coords; % Latent spaces coordinates for pressure

IN.data = noisyVol;
tmp = diffusionMaps(IN);
cVol = tmp.coords; % Latent spaces coordinates for volume

% Normalization 
cPres = cPres ./ std(cPres(1,:));
cVol  = cVol ./ std(cVol(1,:));

figure;

[~,tmp] = procrustes(cPres',cVol'); 
cVolaligned = tmp';
subplot(131); plotData(cPres,colorX); title('Latent space for pressure');
subplot(132); plotData(cVolaligned,colorX); title('Latent space for volume');
subplot(133); plotDif(cPres,cVolaligned,indNoise); title('Differences between latent spaces');


%% MML
  
IN.ksigma = 10; IN.kM = 10; IN.mu = 1.1;
%%%%%% Optimized hyperparameters %%%%%%
% kM = 0 (no sparsification) -> ksigma = 10, mu = 0.1
% kM = 3 -> ksigma = 10, mu = 0.9
% kM = 5 -> ksigma = 10, mu = 1
% kM = 10 -> ksigma = 10, mu = 1.1

IN.data = {noisyPres',noisyVol'};
OUT = MML(IN);
cPresMML = OUT.coords{1}; cVolMML = OUT.coords{2};

cPresMML = cPresMML ./ std(cPresMML(1,:));
cVolMML = cVolMML ./ std(cVolMML(1,:));

figure;
subplot(131); plotData(cPresMML,colorX); title('Latent space for pressure');
subplot(132); plotData(cVolMML,colorX);  title('Latent space for volume');
subplot(133); plotDif(cPresMML,cVolMML,indNoise); title('Differences between latent spaces');


%% Display function
function plotData(C,colorX)

hold on;
scatter3(C(1,:),C(2,:),C(3,:),100,colorX,'filled');
set(gca,'FontWeight','Bold','FontSize',12);
xlabel('1^{st} dim'); ylabel('2^{nd} dim'); zlabel('3^{rd} dim');
legend off; axis([-2.5 2.5 -2.5 2.5]); axis square;

end

function plotDif(C1,C2,indNoise)

hold on;
for i = 1 : size(C1,2)
    plot([C1(1,i),C2(1,i)],[C1(2,i),C2(2,i)],'k','LineWidth',4);
    
    if ismember(i,indNoise)
        plot([C1(1,i),C2(1,i)],[C1(2,i),C2(2,i)],'r','LineWidth',4);        
    end
end
set(gca,'FontWeight','Bold','FontSize',12);
xlabel('1^{st} dim'); ylabel('2^{nd} dim'); zlabel('3^{rd} dim');
axis([-2.5 2.5 -2.5 2.5]); axis square;
end



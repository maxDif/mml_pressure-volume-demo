function OUT = diffusionMaps(IN)


%% Algo Coifman_2006: Diffusion Maps 
% Input arguments:
%     - IN.data: nbDims x nbCases
%     - IN.ksigma
%     - IN.t: diffusion time
%     - IN.alphaT: first normalization parameter / 1=anisotropic kernel (Laplace-Beltrami) / 0.5=Fokker-Planck / 0=normalized graph Laplacian
% Output arguments:
%     - OUT.eigVecs: eigenvectors
%     - OUT.eigVals: eigenvalues
%     - OUT.coords: latent spaces coordinates nbDims x nbCases
%     - OUT.K: Normalized affinity matrix
%     - OUT.Kold: Non-normalized affinity matrix 
%     - OUT.sigmaUSED: kernel bandwidth

if IN.numS ~= size(IN.data,2)
    IN.data = IN.data';
end

[Np,Ns] = size(IN.data);

%% Step #1

tmpKS = squareform(pdist(IN.data'));

if isfield(IN,'alphaT')
    alphaT = IN.alphaT;
else    
    alphaT = 0;
end

if ~isfield(IN,'kM')
    IN.kM = 0;
end

if IN.kM == 0 % kM = 0 <=> no sparsification (only for MML)
    IN.kM = Ns;
end

if isfield(IN,'sigma')
    sigma = IN.tmpsigma;
else
    
    tmp = tmpKS + diag( Inf(Ns,1) );
% % %     tmpK_T = tmp;
    [BX,IX] = sort(tmp,1,'ascend');
    tmpKS_T = tmpKS;
    tmpKS_T2 = tmpKS; 
    for k=1:Ns
        tmpKS_T(IX(round(0.5*IN.ksigma):Ns,k),k) = Inf;
        tmpKS_T(k,k) = 0;
        
        tmpKS_T2(IX(IN.kM:Ns,k),k) = Inf; % Sparsified with kM (only for MML)
        tmpKS_T2(k,k) = 0;
    end
    tmpB = BX(IN.ksigma,:);
    sigma = mean(tmpB(:)); 
    clear tmp tmpB;
end

K = exp(-tmpKS.^2 / (2*sigma.^2));
K_T  = exp(-tmpKS_T.^2 / (2*sigma.^2));
K_T2 = exp(-tmpKS_T2.^2 / (2*sigma.^2)); % only for MML

K_T = (K_T + K_T') / 2;
K_T2 = (K_T2 + K_T2') / 2; % only for MML

OUT.K_T = K_T;
OUT.K_T2 = K_T2;

clear tmpI tmpKS;
Kold = K^(IN.t);


%% Step #2

if alphaT ~= 0
    p = (sum(K,1).^alphaT)';
    K = K ./ (p * p'); %% First normalization
end
p = sqrt(sum(K, 1))';
K = K ./ (p * p'); %% Second normalization = normalized graph Laplacian (proba)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % % % K = diag(p)^-1 * K * diag(p)^-1; %% EQUIVALENT
% % % % solving L f = lambda D f
% % % % means (1-lambda) D f = W f 
% % % % means (1-lambda) f = D^-1/2 W D^-1/2 f
%%% REMARK: This is a symmetrized version of K(x,y)/p(x) ie. a(x,y)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

K = K^(IN.t);
numDimMax = IN.numDimMax;

if isfield(IN,'W')
    K = IN.W;
end

[eV,eE] = eig(K); %%% ORDERED FROM SMALLEST TO LARGEST
eV = fliplr(eV);
eE = flipud(diag(eE));
eV = eV(:,2:end);
eE = eE(2:end);

eV = eV(:,1:numDimMax);
eE = eE(1:numDimMax);

%% Step #3

OUT.IX = IX;
OUT.K = K;
OUT.Kold = Kold;
OUT.K_T = K_T;
OUT.eigVals = eE;
OUT.eigVecs = eV;

lambda_t = eE.^IN.t; 
lambda_t=ones(Ns,1)*lambda_t';

tmpC = zeros(Ns,Ns);
tmpC(1:Ns,1:numDimMax) = eV.*lambda_t;
OUT.coords = tmpC';

OUT.sigmaUSED = sigma;

end
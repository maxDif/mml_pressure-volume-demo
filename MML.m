function OUT = MML(IN)

%% Algo Valencia_2011: Multiple manifold learning

% Input arguments:
%     - IN.data: cell x nb descriptors (in cell: nbCases x nbDims)
%     - IN.mu: interactions weight
%     - IN.alphaT: first normalization parameter / 1 = anisotropic kernel (Laplace-Beltrami) / 0.5=Fokker-Planck / 0=normalized graph Laplacian
%%%%%%%%%% option #1: re-use the results of previous diffusion maps
%     - IN.W: previous kernels
%%%%%%%%%% option #2: re-compute diffusion maps from the data
%     - IN.ksigma: required for diffusion maps
%     - IN.numS: min size of the data
% Output arguments:
%     - OUT.eigVecs: eigenvectors
%     - OUT.eigVals: eigenvalues
%     - OUT.coords: latent spaces coordinates cell x nb descriptors (in cell: nbDims x nbCases)
%     - OUT.A: affinity matrix (normalized)
%     - OUT.W: affinity matrix for one descriptor (one cell for each)
%     - OUT.M: extradiagonal matrix

Nfeat = length(IN.data);
[Nk,Np] = size(IN.data{1});

if isfield(IN,'facT')
    facT = IN.facT;
else
    facT = 1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% W_T: used for diagonal matrices
% W_T2: used for extra-diagonal matrices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% using precomputing W or not
if isfield(IN,'W')
    W_T = IN.W;
    if IN.kM == 0 % no sparsification 
        W_T2 = W_T;
        for s=1:Nfeat
            tmpW_T = IN.W{s};
            [~,IX] = sort(tmpW_T - diag(diag(tmpW_T)),1,'descend'); % Set to 0 the diag
            tmpInd{s} = IX;
        end
    else
        
        for s=1:Nfeat
            tmpW_T = IN.W{s};
            tmpW_T2 = IN.W{s};
            [~,IX] = sort(tmpW_T - diag(diag(tmpW_T)),1,'descend'); % Set to 0 the diag
%             IX = flipud(IX);
            for k=1:Nk
                tmpW_T(IX(round(0.5*IN.ksigma):Nk,k),k) = 0;
                tmpW_T(k,k) = 1;           
                
                tmpW_T2(IX(IN.kM:Nk,k),k) = 0;
                tmpW_T2(k,k) = 1;
            end
            
            tmpW_T = (tmpW_T + tmpW_T') / 2;
            tmpW_T2 = (tmpW_T2 + tmpW_T2') / 2;

            W_T{s} = tmpW_T;
            W_T2{s} = tmpW_T2;
            tmpInd{s} = IX;
        end
        
    end
    
else
    tmpIN = IN;
    for s=1:Nfeat
        X = IN.data{s};
        tmpIN.data = X';
        tmpOUT = diffusionMaps(tmpIN);
        W{s} = tmpOUT.Kold;
        if IN.kM == 0
            W_T{s} = tmpOUT.K;
            W_T2{s} = tmpOUT.K;
        else
            W_T{s} = tmpOUT.K_T; %K_T
            W_T2{s} = tmpOUT.K_T2;
        end
        
        tmpInd{s} = tmpOUT.IX;
    end
end
W = W_T;

%% Computing the global MML matrix 
M = cell(Nfeat,Nfeat);
A0 = zeros(Nfeat*Nk,Nfeat*Nk);

A = zeros(Nfeat*Nk,Nfeat*Nk);
for s=1:Nfeat
    for q=1:Nfeat
        if q==s
            idx = (1:Nk) + Nk*(s-1);
            A(idx,idx) = W{s};
            M{s,s} = W{s};
            
            A0(idx,idx) = W{s};
        else
            tmp = ( repmat( sum(W_T2{s}.^2,1).^(1/2) , [Nk,1] )' .* repmat( sum(W_T2{q}.^2,1).^(1/2) , [Nk,1] ) );
            M{s,q} = ( W_T2{s}' * W_T2{q} ) ./ tmp;
            

            idx_s = (1:Nk) + Nk*(s-1);
            idx_q = (1:Nk) + Nk*(q-1);
            A(idx_s,idx_q) = facT*IN.mu*M{s,q};
            A(idx_q,idx_s) = facT*IN.mu*M{s,q}';
            
            A0(idx_s,idx_q) = M{s,q};
            A0(idx_q,idx_s) = M{s,q}';
        end
    end
end

Aold = A;

if IN.alphaT ~= 0
    p = (sum(A,1).^IN.alphaT)';
    A = A ./ (p * p'); %% First normalization
end
p = sqrt(sum(A, 1))';
A = A ./ (p * p'); %% Second normalization = normalized graph Laplacian (proba)

[eV,eE] = eig(A); %%% ORDERED FROM SMALLEST TO LARGEST
eV = fliplr(eV);
eE = flipud(diag(eE));
eV = eV(:,2:end);
eE = eE(2:end);

OUT.eigVals = eE;
OUT.eigVecs = eV;
OUT.A = A;
OUT.A0 = A0;
OUT.M = M;
OUT.W = W;
OUT.tmpInd = tmpInd;

Ns_big = size(A,1);
lambda_t = ones(Ns_big,1)*eE';

tmpC = eV.*lambda_t;
tmpC = tmpC';
tmpC(Nk+1:end,:) = 0;

OUT.coords = cell(Nfeat,1);
for s=1:Nfeat
    idx = (1:Nk) + Nk*(s-1);
    tmp = tmpC(:,idx);
    OUT.coords{s} = tmp;
end

end

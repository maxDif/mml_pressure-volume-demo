# Characterizing interactions between cardiac shape and deformation by non-linear manifold learning
 
 This is the implementation of the method used for the following paper: https://doi.org/10.1016/j.media.2021.102278

 ## Database 

The database is composed of simulation pressure and volume curves across one cycle of the left ventricle made by a 0D model.  
 The  model  is  written  in  CellML and  can  be launched in the OpenCor modelling platform ([https://models.cellml.org/workspace/44c](https://models.cellml.org/workspace/44c)). 
 We used a script version provided by the authors that facilitates repeated  runnings  of  the  model  with  different  sets
of parameters: the relaxation rate and the max contraction (Molero et al., [10.1007/s10237-017-0960-0](10.1007/s10237-017-0960-0)). 

The database is composed of 2 dataset:
- the first one have 130 simulations with varying value of the relaxation rate and the max contraction
- the second one is the same except for 9 points in hwich we added noise by changing the value of a third parameter randomly


 ## Code 

This repository contains an implementation of the dimensionality reduction method Diffusion maps described by Coifman et al. ([10.1016/j.acha.2006.04.006](10.1016/j.acha.2006.04.006)) and our implementation of Multiple Manifold Learning (MML) generalized in  Clough et al. ([10.1109/TPAMI.2019.2891600](10.1109/TPAMI.2019.2891600)) and described in our paper.
The main.m file contain a script to execute the method of the noisy dataset with set hyperparameters and a display of the results.
